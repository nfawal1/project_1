# importer the packages
import pandas as pd
pd.set_option('max_columns', None)
import json
from sklearn.linear_model import Lasso
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.linear_model import Lasso

# Declare the path for inputs and outputs
PATH = "/Users/nizar/Downloads/data_science_3/"

# read input files
campaign_impressions_json = [json.loads(line) for line in open( PATH + "campaign_impressions.json", 'r')]
user_segments = pd.read_csv(PATH + "user_segments.csv")


campaign_impressions = pd.json_normalize(campaign_impressions_json)

# on regarde la distribution des valeurs dans le dataset campaign_impressions
# Figure_1
quan = list(campaign_impressions.loc[:, campaign_impressions.dtypes != 'object'].columns.values)
grid = sns.FacetGrid(pd.melt(campaign_impressions, value_vars=quan),
                     col='variable', col_wrap=4, height=3, aspect=1,
                     sharex=False, sharey=False)
grid.map(plt.hist, 'value', color="steelblue")
plt.show()
# on peut voir que:
# on a taux de conversion = 33%
# on a un seul client (respo.oi avec une seul valeur = 8.0)
# 3 campagnes publicitaires  (resp.c)
# 2 systems d'exploitation (dev.os)
# donc 2 sources de données (dev.sid)
# 3 applications surlesquelles la publicité a été diffusées
# 10 contenus diffusé 
# on regarde la distribution des valeurs dans le dataset campaign_impressions
# transform user_segments to int
columns2int = ["ANIMAL","CAR_OWNER","OFFICE_WORKER","GARDEN","PARENT"]
user_segments[columns2int]= user_segments[columns2int].astype(int)

quan = list(user_segments.loc[:, user_segments.dtypes != 'object'].columns.values)
grid = sns.FacetGrid(pd.melt(user_segments, value_vars=quan),
                     col='variable', col_wrap=4, height=3, aspect=1,
                     sharex=False, sharey=False)
grid.map(plt.hist, 'value', color="steelblue")
plt.show()
# les segments clients sont repartie en 60% non et 40% oui

# on regarde la correlation lineaire basique 2x2
# figure_3 pour user_segments

sns.heatmap(user_segments._get_numeric_data().astype(float).corr(),
            square=True, cmap='RdBu_r', linewidths=.5,
            annot=True, fmt='.2f').figure.tight_layout()
plt.show()
# il y a une correlation faible entre les variables
#figure_4 pour les impressions
sns.heatmap(campaign_impressions._get_numeric_data().astype(float).corr(),
            square=True, cmap='RdBu_r', linewidths=.5,
            annot=True, fmt='.2f').figure.tight_layout()
plt.show()
# il y a une correlation faible entre les variables, sauf pour l'os avec 24%

# rename column Unnamed: 0 for better coherence with the log file
user_segments.rename(columns={'Unnamed: 0':"uuid"}, inplace = True)

# transform the log file to df, on supprime les 529 lignes avec un resp.cr null et on le transforme en int
campaign_impressions = pd.json_normalize(campaign_impressions_json)
campaign_impressions.dropna(subset=["resp.cr"],inplace=True)
campaign_impressions["resp.cr"]= campaign_impressions["resp.cr"].astype(int)

# transform the ts column into date and time
campaign_impressions["date_tmp"] = pd. to_datetime(campaign_impressions['ts'], unit='s')
campaign_impressions['date'] = [d.date() for d in campaign_impressions['date_tmp']]
campaign_impressions['time'] = [d.time() for d in campaign_impressions['date_tmp']]
campaign_impressions.drop("date_tmp", inplace = True, axis=1)
# the date column has only one value "2017-12-01"
# maybe we can bin the time column into morning / noon / afternoon / night categories

# cast uuid to numeric
campaign_impressions["uuid"] =pd.to_numeric(campaign_impressions["uuid"])
campaign_impressions = campaign_impressions.set_index("uuid")
user_segments["uuid"] =pd.to_numeric(user_segments["uuid"])
#print(campaign_impressions["date"].value_counts())
# merge the 2 dfs
df = user_segments.join(campaign_impressions, on = "uuid", how = "outer" )


# on a autant de client par campagne et contenu
# pareil pour les utilisateurs des applications et des os
# donc le ciblage est fait de maniere equilibré à travers les differents canaux
print(df.groupby(["resp.c","resp.cr","dev.app","dev.os"])["uuid"].nunique())
"""
resp.c  resp.cr
1       1          244
        2          247
        3          219
        4          237
        5          253
        6          220
        7          241
        8          228
        9          208
        10         235
2       1          230
        2          227
        3          254
        4          211
        5          232
        6          252
        7          240
        8          244
        9          242
        10         238
3       1          235
        2          227
        3          233
        4          221
        5          224
        6          237
        7          237
        8          233
        9          249
        10         239
Name: uuid, dtype: int64
"""
########################################################################################################################
########################################## Are the features correlated and of importance ?##############################
########################################################################################################################
# on regarde la correlation entre les variables et la target
#print(df.corr(method='pearson').iloc[7].sort_values(ascending=False))
"""
conv             1.000000
dev.os           0.236527
ts               0.131528
PARENT           0.104041
ANIMAL           0.030863
uuid             0.014485
resp.cr          0.002611
dev.app         -0.000693
dev.sid         -0.004841
GARDEN          -0.011554
CAR_OWNER       -0.024685
OFFICE_WORKER   -0.065249
resp.c          -0.106978
resp.oi               NaN
"""


df2 =df.drop(["date","time","ts","resp.oi","resp.c","resp.cr","dev.os","dev.sid","dev.app"], axis=1).drop_duplicates()
X  = df2.drop(["conv"], axis=1)
y = df2["conv"]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.3, random_state=42)

# Instantiate a lasso regressor: lasso
lasso = Lasso(alpha=0.4, normalize=True)
# Fit the regressor to the data
lasso.fit(X, y)

# Compute and print the coefficients
lasso_coef = lasso.coef_
print(lasso_coef)
# [ 0.  0. -0. -0. -0.  0.  0.  0. -0.  0. -0. -0.]
# A zero coefficient indicates no relationship between the predictor and the response, since zero multiplied by anything is zero.

########################################################################################################################

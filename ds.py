# importer the packages
import pandas as pd
pd.set_option('max_columns', None)
import numpy as np
import json
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, confusion_matrix, recall_score, roc_auc_score, precision_score,classification_report,f1_score
from imblearn.over_sampling import SMOTE
from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV, ShuffleSplit, cross_val_predict
from xgboost import XGBClassifier, XGBRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
import pickle
from sklearn.ensemble import RandomForestClassifier

# Declare the path for inputs and outputs
PATH = "/Users/nizar/Downloads/data_science_3/"
PATH_LOGS = PATH + "campaign_impressions.json"
PATH_SEGS = PATH + "user_segments.csv"


def process_logs(path_logs):
    """
    Funtion to read the impression logs and clean the data
    :param PATH_LOGS: path the impression logs
    :return: cleand df
    """
    # read input files
    input_logs_json = [json.loads(line) for line in open( path_logs, 'r')]
    # transform the log file to df, remove 529 lignes where un resp.cr is null and transform to int
    input_logs = pd.json_normalize(input_logs_json)
    input_logs.dropna(subset=["resp.cr"],inplace=True)
    input_logs["resp.cr"]= input_logs["resp.cr"].astype(int)

    # transform the ts column into date and time
    input_logs["date_tmp"] = pd.to_datetime(input_logs['ts'], unit='s')
    input_logs['date'] = [d.date() for d in input_logs['date_tmp']]
    input_logs['time'] = [d.time() for d in input_logs['date_tmp']]
    input_logs.drop("date_tmp", inplace = True, axis=1)
    # the date column has only one value "2017-12-01"
    # maybe we can bin the time column into morning / noon / afternoon / night categories

    # cast uuid to numeric
    input_logs["uuid"] =pd.to_numeric(input_logs["uuid"])
    input_logs = input_logs.set_index("uuid")
    # remove resp.oi since there is only one value
    input_logs.drop("resp.oi",axis=1,inplace = True)

    return input_logs


def process_segments (path_segs):
    """

    :param PATH_SEGS: path to the segments files
    :return: cleaned df
    """
    # read input file
    segments = pd.read_csv(path_segs)
    # transform user_segments to int
    columns2int = ["ANIMAL","CAR_OWNER","OFFICE_WORKER","GARDEN","PARENT"]
    segments[columns2int]= segments[columns2int].astype(int)

    # rename column Unnamed: 0 for better coherence with the log file
    segments.rename(columns={'Unnamed: 0':"uuid"}, inplace = True)

    segments["uuid"] = pd.to_numeric(segments["uuid"])
    return segments


# merge the 2 dfs
user_segments = process_segments(PATH_SEGS)
campaign_impressions = process_logs(PATH_LOGS)
df = user_segments.join(campaign_impressions, on="uuid", how="outer" )

df2 = df.drop(["date","time","ts"], axis=1).drop_duplicates()
df_train = df2[df2["resp.c"]==1]
X = df2.drop(["conv"], axis=1)
y = df2["conv"]

#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=42, stratify=y)
X_train, X_test, y_train, y_test = train_test_split(X, y)
smote = SMOTE(random_state = 14)
X_train_3, y_train_3 = smote.fit_resample(X_train, y_train)
"""
y_train_3.value_counts().plot(kind='bar')
plt.title('label balance')
plt.xlabel('label values')
plt.ylabel('amount per label')
plt.show()
"""
########################################################################################################################
########################################## KNN #########################################################################
########################################################################################################################


def find_nb_k(X_train, X_test, y_train, y_test):
    # Setup arrays to store train and test accuracies
    neighbors = np.arange(1, 9)
    train_accuracy = np.empty(len(neighbors))
    test_accuracy = np.empty(len(neighbors))

    # Loop over different values of k
    for i, k in enumerate(neighbors):
        # Setup a k-NN Classifier with k neighbors: knn
        knn = KNeighborsClassifier(n_neighbors=k)

        # Fit the classifier to the training data
        knn.fit(X_train,y_train)

        #Compute accuracy on the training set
        train_accuracy[i] = knn.score(X_train, y_train)

        #Compute accuracy on the testing set
        test_accuracy[i] = knn.score(X_test, y_test)
    return train_accuracy, test_accuracy, neighbors


def plot_knn(train_accuracy, test_accuracy,neighbors):
    # Generate plot
    plt.title('k-NN: Varying Number of Neighbors')
    plt.plot(neighbors, test_accuracy, label = 'Testing Accuracy')
    plt.plot(neighbors, train_accuracy, label = 'Training Accuracy')
    plt.legend()
    plt.xlabel('Number of Neighbors')
    plt.ylabel('Accuracy')
    plt.show()

#train_accuracy, test_accuracy,neighbors = find_nb_k(X,y)
#plot_knn(train_accuracy, test_accuracy,neighbors)

# Instantiate a k-NN classifier: knn
def knn_k(X_train, X_test, y_train, y_test,k):
    knn = KNeighborsClassifier(n_neighbors=k)

    # Fit the classifier to the training data
    knn.fit(X_train,y_train)

    cv_scores = cross_val_score(knn, X_train, X_train, cv=10, scoring="roc_auc")
    #print mean cv scores
    cv_scores1 = cross_val_score(knn, X_train, y_train, cv=10, scoring="accuracy")
    cv_scores2 = cross_val_score(knn, X_train, y_train, cv=10, scoring="recall")
    cv_scores3 = cross_val_score(knn, X_train, y_train, cv=10, scoring="roc_auc")
    print('cv_scores acc:{}'.format(np.mean(cv_scores1)))
    print('cv_scores recall:{}'.format(np.mean(cv_scores2)))
    print('cv_scores auc:{}'.format(np.mean(cv_scores3)))

    # Predict the labels of the test data: y_pred
    y_pred = knn.predict(X_test)
    #y_pred = cross_val_predict(knn, X, y, cv=3)
    p_train = knn.predict(X_train)
    # Generate the confusion matrix and classification report
    print(confusion_matrix(y_test, y_pred))
    print(classification_report(y_test, y_pred))

    # Print the accuracy
    print('train f1 score')
    print(f1_score(y_train, p_train ,average='micro'))
    print('test f1 score')
    print(f1_score(y_test, y_pred ,average='micro'))
print("KNN: ")

def knn_seach (X_train, X_test, y_train, y_test):
    estimator_KNN = KNeighborsClassifier(algorithm='auto')
    parameters_KNN = {
    'n_neighbors': (1,10, 1),
    'leaf_size': (20,40,1),
    'p': (1,2),
    'weights': ('uniform', 'distance'),
    'metric': ('minkowski', 'chebyshev'),
    }

    # with GridSearch
    grid_search_KNN = GridSearchCV(
    estimator=estimator_KNN,
    param_grid=parameters_KNN,
    scoring = 'accuracy',
    n_jobs = -1,
    cv = 5
    )
    KNN_1=grid_search_KNN.fit(X_train, y_train)
    y_pred_KNN1 =KNN_1.predict(X_test)

    #Parameter setting that gave the best results on the hold out data.
    print(grid_search_KNN.best_params_ )
    #Mean cross-validated score of the best_estimator
    print('Best Score - KNN:', grid_search_KNN.best_score_ )

#knn_seach(X_train_3, X_test, y_train_3, y_test)


########################################################################################################################
########################################## LogReg #########################################################################
########################################################################################################################
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import roc_curve
def logRegressor (X_train, X_test, y_train, y_test):
    # Create the classifier: logreg
    logreg = LogisticRegression(max_iter=1000,solver='lbfgs')

    # Fit the classifier to the training data
    logreg.fit(X_train,y_train)
    cv_scores1 = cross_val_score(logreg, X_train, y_train, cv=10, scoring="accuracy")
    cv_scores2 = cross_val_score(logreg, X_train, y_train, cv=10, scoring="recall")
    cv_scores3 = cross_val_score(logreg, X_train, y_train, cv=10, scoring="roc_auc")

    #print each cv score (accuracy) and average them
    print('cv_scores acc:{}'.format(np.mean(cv_scores1)))
    print('cv_scores recall:{}'.format(np.mean(cv_scores2)))
    print('cv_scores auc:{}'.format(np.mean(cv_scores3)))

    # Predict the labels of the test set: y_pred
    y_pred = logreg.predict(X_test)
    p_train = logreg.predict(X_train)
    # Compute and print the confusion matrix and classification report
    print(confusion_matrix(y_test, y_pred))
    print(classification_report(y_test, y_pred))
    print('train f1 score: ',f1_score(y_train, p_train ,average='micro'))
    print('test f1 score: ',f1_score(y_test, y_pred ,average='micro'))
    return logreg


def generate_roc(model,X_test,y_test ):
    # Compute predicted probabilities: y_pred_prob
    y_pred_prob = model.predict_proba(X_test)[:,1]

    # Generate ROC curve values: fpr, tpr, thresholds
    fpr, tpr, thresholds = roc_curve(y_test, y_pred_prob)

    # Plot ROC curve
    plt.plot([0, 1], [0, 1], 'k--')
    plt.plot(fpr, tpr)
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC Curve')
    plt.show()
#generate_roc(logRegModel,X_test,y_test)

"""
THRESHOLD = 0.30
preds = np.where(logRegModel.predict_proba(X_test)[:,1] > THRESHOLD, 1, 0)
unique, counts = np.unique(preds, return_counts=True)
print(dict(zip(unique, counts)))
print(y_test.value_counts())
print(pd.DataFrame(data=[accuracy_score(y_test, preds), recall_score(y_test, preds),
                   precision_score(y_test, preds), roc_auc_score(y_test, preds)],
             index=["accuracy", "recall", "precision", "roc_auc_score"]))
print(confusion_matrix(y_test, preds))
"""


def classification(X_tr,y_tr,X_te,y_te,method):

    method.fit(X_tr,y_tr)
    cv_scores1 = cross_val_score(method, X_train, y_train, cv=10, scoring="accuracy")
    cv_scores2 = cross_val_score(method, X_train, y_train, cv=10, scoring="recall")
    cv_scores3 = cross_val_score(method, X_train, y_train, cv=10, scoring="roc_auc")
    print('cv_scores acc:{}'.format(np.mean(cv_scores1)))
    print('cv_scores recall:{}'.format(np.mean(cv_scores2)))
    print('cv_scores auc:{}'.format(np.mean(cv_scores3)))

    p_train = method.predict(X_tr)
    p_test = method.predict(X_te)
    print(confusion_matrix(y_te, p_test))
    print(classification_report(y_te, p_test))

    print('train f1 score')
    print(f1_score(y_tr, p_train ,average='micro'))
    print('test f1 score')
    print(f1_score(y_te, p_test ,average='micro'))
    print('-'*20)
print("xgbc")
xgbc=XGBClassifier(random_state=14)
#classification(X_train_3, y_train_3, X_test, y_test, xgbc)

print("random forest")
regressor_forest=RandomForestClassifier(n_estimators=100,random_state=1)
#classification(X_train_3, y_train_3, X_test, y_test, regressor_forest)


print("logReg")
#logRegModel = logRegressor (X_train_3, X_test, y_train_3, y_test)

print("knn")
#knn_k(X_train_3, X_test, y_train_3, y_test,5)


# test the model on an np.array
toPredict = np.array(['013','0','0','0','0','1','1','3','5','26','7']).astype(int)
print(logRegModel.predict(toPredict.reshape(1,-1)))
print(logRegModel.predict_proba(toPredict.reshape(1,-1)))


# serialize our model and save it in the file logRegModel.pickle
print ("Model trained. Saving model to logRegModel.pickle")
with open("logRegModel.pickle", "wb") as file:
    pickle.dump(logRegModel, file)
print ("Model saved.")

# Project 1 : Class / prediction
There are 2 objectives for this project:
- Define a class object to represent the data present in each log in order to facilitate the handling of said logs
- Set up a prediction model to predict the probability that an impression will lead to an in-store conversion

# Steps taken : 
## Explore the data : exploration.py
The exploration.py script was written to explore the data provided for this project, bellow are the most significant insights:
- 500 unique users
- 5 user segments where the ratio for each is 60/40 for no/yes
- 10 000 log impressions
- 1 client for whom the campaign id was run
- 3 campaigns
- 10 advert contents spread evenly accros each campaing
- 2 device types and thus 2 data source types and 3 apps spread evenly accros the campaigns
- a conversion rate of 30% which leads to imbalaced data

In general the data seems relatively clean with only 5% of the impression logs having null advert content. 
Furthermore, the features don't seem to be correlated (figure 3 and 4).

## Define the class : class.py
The class.py script is based on the class givin in the task description and adds 3 new functions:
- userHasSegment : Returns True if UUID associated with hit is marked as being in the given segment
- addSegments: add user segments to log impression object
- returnValue : return the different attributes of the impression in a specific order

This script allows to convert the log impression json to a array of impression objects add the user segments to the each impression.
Once the model is defined, the predict_from_log function takes in account 3 arguments:  
- a log impression object
- a column list to sort the values in the same order as the trained model
- the prediction model 
The output of the function is the prediction result as well as the probability that it will lead to an in-store conversion.

## Model selection : ds.py
Dataframes were used in this section since it's was easier to handel all the data and use it to train the predictors.
A function was defined to process each input file and prepare them. 
SMOTE was used to balance the target variables in order to get best resutls out of the predictors. 
Several predictors were tested:
- Knn 
- Logistic Regression
- Random Forest
- XgBoost

A summary of the main performance metrics of these predictors can be found in Figure 6.
Accuracy is not the best indicator of performance du to the unbalanced target variables. For this reason, recall, F1 score and Roc-AUC metrics were also used to evaluate the predictors. As we can see Random forest gives the best results for Roc-AUC, however we can see that it overfit on the training data since the F1 score goes from 99% on the training to 61 on the test data. I chose to implement the Logistic regression model instead of random forest in the class prediction script since it was the predictors the second best records for these metrics on the train and test data as well as since we're able to change the threshold of the probability to define the target prediction.

# Review: Reflect on your work for the above and how you would improve it given more time.
The aim of this task is to identify your ability to think creatively and reflectively about problems.
Some specific questions to consider include:

• Do you think your class is extensible to other problems using the same data?
Once all the data is processsed through the class created, it can be used to mesure the performance of the campaign,
the content used to predict which are the most relevant, identify which os, app and data source are the most relevant
for each type of campaign in order to adapt the targeting strategy

• How would you test the functionality you have developed for your class?
Unitary tests can be used to check the validaty of each functionnality, for exemple for the didConvert function,
a call to the function with a predefined and known entry and test if the result  is coherent with the input

• Do you think your developed model could be put into production? Why?
No, it's was trained on relatively a small sample and therefore it didn't yeald the best results

• Why have you chosen the performance metric(s) that you have reported?
Several metrics were used to evaluate the model:
- A confusion matrix by counting the missclassified prediction and detect TP, FP, TN, FP
- the data generated from this confusion matrix was then used to generate :
    - accuracy
    - recall
    - precision
    - F1 score
    - Roc-auc curve
- In order the get more accurate results, cross validation split was used in order to make sure the balance between train et test sets.

• What external data-sets do you think would enrich the performance of your model?
- first of all, more date points would be very helpful to enhance the model
- Second, having information about the type of product for the campaign can be useful in creating a portfolio
for each category of campaign as well as their customers
- Data for older campaigns can also be used to enrich the model and maybe used to reiforcement learning
- More data on the clients, such as socio-demographic data can also be used to increase the performance of the model





import pandas as pd
pd.set_option('max_columns', None)
import numpy as np
import json
import pickle

PATH = "/Users/nizar/Downloads/data_science_3/"
PATH_LOGS = PATH + "campaign_impressions.json"
PATH_SEGS = PATH + "user_segments.csv"
colList = ['uuid', 'segments.ANIMAL', 'segments.CAR_OWNER', 'segments.GARDEN', 'segments.OFFICE_WORKER',
           'segments.PARENT', 'resp.cr', 'resp.c', 'dev.os', 'dev.sid', 'dev.app']

input_logs_json = [json.loads(line) for line in open( PATH_LOGS, 'r')]

# Import our model
with open('logRegModel.pickle', "rb") as file:
    logRegModel = pickle.load(file)

def process_segments (path_segs):
    """

    :param PATH_SEGS: path to the segments files
    :return: cleaned df
    """
    # read input file
    segments = pd.read_csv(path_segs)
    # transform user_segments to int
    columns2int = ["ANIMAL","CAR_OWNER","OFFICE_WORKER","GARDEN","PARENT"]
    segments[columns2int]= segments[columns2int].astype(int)

    # rename column Unnamed: 0 for better coherence with the log file and cast as str
    segments.rename(columns={'Unnamed: 0':"uuid"}, inplace = True)
    segments["uuid"]= segments["uuid"].astype(str).apply(lambda x: x.zfill(3))
    return segments

user_segments = process_segments(PATH_SEGS)

class Impression(object):
    """
    Class to simplify interacting with an impression log
    """
    def __init__(self,log):
        """
        Parameters
        ----------
        log : dict
        A single row of the impression log file
        """
        self._log = log
        self.uuid = log.get('uuid')
        self.segments = {}

    def didConvert(self):
            return bool(self._log['conv'])

    def userHasSegment(self,segment_name):
        """
        Returns True if UUID associated with hit is marked as being in the given segment
        """
        return self.segments[segment_name] == 1

    def addSegments(self,segments_df ):
        """
        Function to add user segments to log impression object
        :arg :
            self : the log impression object
            segments_df : the df with the segments per user
        :returns : the log impression object with a new attribute "segments" with all the segment attributes
        """
        columnList = segments_df.drop("uuid", axis=1).columns
        for col in columnList:
            seg_dict = {col:segments_df[segments_df["uuid"] == self.uuid][col].values[0]}
            self.segments.update(seg_dict)

    def returnValue(self, attribute):
        """
        Function to return the different attributes of the impression in a specific order
        :param segment: attribute name
        :return: attribute value
        """
        if len(attribute.split("."))>1:
            col1 = attribute.split(".")[0]
            col2 = attribute.split(".")[1]
            if col1 != "segments" :
                return log.get(col1)[col2]
            else:
                return self.segments.get(col2)
        else :
            return log.get(attribute)



# transform the json file into an array of impression objects
log_objects = []
for log in input_logs_json:
    log_object = Impression(log)
    log_objects.append(log_object)
    #log_object.addSegments(user_segments)

log2 = log_objects[2]
log2.addSegments(user_segments)


def predict_from_log (colList, log_object,predictor):
    """
    Transform the impression object to an np array in order to send it through the prediction model
    :param : list of columns, the impression object, the prediction model
    :return : and array of two elements:
                    - the prediction result as 1 or 0
                    - an array of the probability of 0 and 1 respectively

    """
    log_arr =[]
    for col in colList:
        log_arr.append(log_object.returnValue(col))


    prediction = (logRegModel.predict(np.array(log_arr).astype(int).reshape(1,-1)))[0]
    prediction_probla = (logRegModel.predict_proba(np.array(log_arr).astype(int).reshape(1,-1)))
    return prediction, prediction_probla


# test userHasSegment function
print(log2.userHasSegment("PARENT"))

# get prediction and proba for an impression
print(predict_from_log(colList, log2,logRegModel))


